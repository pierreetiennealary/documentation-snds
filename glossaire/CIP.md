# CIP - Code Identifiant de Présentation
<!-- SPDX-License-Identifier: MPL-2.0 -->


## Ailleurs dans la documentation
- Fiche thématique sur les [médicaments](../fiches/medicament.md)

## Références
- [Club InterPharmaceutique](https://www.cipmedicament.org/)
- [Correspondance CIP/UCD](https://smt.esante.gouv.fr/terminologie-cip_ucd/)
